package com.programming;

public class MyDecompress {

    public String decompress(String user_input){
        StringBuilder output = new StringBuilder();
        int startIndex = 0;
        decompress(user_input, startIndex, output);
        return output.toString();
    }

    public int decompress(String text_input,int  index_text,StringBuilder output_text){

        StringBuilder counts = new StringBuilder();
        StringBuilder text = new StringBuilder();
        char startString = '[';
        char stopString = ']';

        int index = index_text;

        while (index < text_input.length()){
            while (Character.isDigit(text_input.charAt(index))){
                counts.append(text_input.charAt(index));
                index++;
            }

            if (text_input.charAt(index) == startString && counts.length() > 0){
                if (text.length() > 0){
                    output_text.append(text);
                    text = new StringBuilder();
                }

                index = decompress(text_input, index + 1, text);

                int repeats = Integer.parseInt(counts.toString());

                output_text.append(text.toString().repeat(repeats));

                text = new StringBuilder();
                counts = new StringBuilder();

            } else if (text_input.charAt(index) == stopString) {
                return index + 1;
            } else {
                output_text.append(text_input.charAt(index));
                index++;
            }
        }
        return index;
    }
}
