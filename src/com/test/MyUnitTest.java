package com.test;

import com.programming.MyDecompress;
import org.junit.Test;
import static org.junit.Assert.*;

public class MyUnitTest {
    @Test
    public void testUnit(){

        MyUnitTest myUnit = new MyUnitTest();
        MyDecompress output = new MyDecompress();

        String test1 = output.decompress("2[abc]3[ab]c");
        assertEquals("abcabcabababc", test1);

        String test2 = output.decompress("10[a]c2[ab]");
        assertEquals("aaaaaaaaaacabab", test2);

        String test3 = output.decompress("2[3[a]b]");
        assertEquals("aaabaaab", test3);
    }
}
